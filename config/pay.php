<?php
return [
    'alipay' => [
        'app_id'         => '2016091300502532',
        'ali_public_key' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxBHwAEDyAyKM3q+ILDrbW38nBcbWtPBNiUXWxGwF/i8lA4rdOoj2lJIuWLmion1rmHtTRspaCBvLykUQtCzYmqmKIavKoyOxzQIs+GU/k+y7Op6AXhJSKTFW5V7LXy7Fe46a39XC9N8QelXg//TOL5cHlDsajM+kw8xGbC33cqGnxUAtn03KJXkqP5WHV92y/Oi2Dn6d6Fke3KUFl6qsvHvk9svuF2rVQ2asDBN8SB6gRktUXf7zDMFqpI90CjEbGeT07g3qYC2oCp8BAvkfeglE5EoTIM69rh0igpHcvcsAoS1rt6v5dv5B426I4o9LNBFzKZWqDJCKiZoHUOwYfwIDAQAB',
        'private_key'    => 'MIIEowIBAAKCAQEAw8kKEIN2yKQm5EW9nbUgfw7z4sRzqvSyqK8m9PHdYtFiE6NsLxZfSwSEWfD346y1AoNdGGaLXzvCRvuU+Zy2qMssOMUKtbVyCkzb4PGKPIr5/V1cXys1tBNxeICclYaieUUmJ/i1HC8+9Z7SwlVu8Kvg1pBoLdf9pyxYut/UY6ov/rZz9P6H2Oxujzz3toxDg6OKZQMfoJE1jEZoFWrdnvB13FR3UNaMssDjYYHOJ1nPka9g/ZMLZ6C4jIPnT7OAZ9+yD2iF3BUSenk7H7fmDFzW6gC7zUK1LsYEfwrAgANwPbdmKqrkhAUTdpw3UgHrATK0CYXgHZRneqqfhGfoGwIDAQABAoIBAQC0N8xh4wFmM2PxYBjooJ+nFXZ92OO4C8Kg74lmk3R7z8z90sJ0nZ1eCmxh9iPYmVONWqpJGatVIEb23AIHuq+QtGSiZQ40sS6mOJV1fCEJFcyD4npyx+2JMxSSHaEFKodIJdS3moHrzbmgkm/ino2eUDiIiSi8ekNsrK+IZRy2F4aYMry85KONaGyy8AFsJ4KBN5Qd9p2cbQVRPPfeRwXUVcC/aRsS1v7LiuKn1bf+z8rIg67SPlU3eqbgPpTekpdkCkhwkBDW1RkzsHMUp/7d6FxakFwvYRXsK5ozqoWSMa2QB6BHH+pakQT9dAWEwTQRUU/XkFALhODCyeVLmRFRAoGBAPRHmo0e5dWYQ7sx2RwS7hUItVoeTyxlnJ7PpONhqqVi9J/4fiF26zPZQBSRTLDc0mwN08OzsgZbBhZXweCUSakgZec22Ugsb+pJ/9pHMfI5vnq/g2TZETfEAA2N4YsWpq5C5YqilXPdBrZLWRhyUTXkOzWiPNQ9re42P0jOBNulAoGBAM0tzA73KCZOfkXH0Rqa6h9V/mKQnXGeF4pPecBExtSsaD8kv4UILKQLSmDkbCVx8cTuGVG+gWWB3TBazvTCfntk1Z0SHiJjLPGhUlnOIV0qoDe1nhje+En82U4vbbqswzU+9WaVPyGo45As2FJhIl+ReTUe/8CegJm08tQNyWi/AoGAHPYoUrbpFOvai+UbEoMECyZfKfIR/PeHHCUOLY80xir3ScgSXDFXLWyCdm3Lo+RqZjk1dCxn3O0JybhZ2iB6ozyeGE2ecTojgqmHVf++4jMKbE8satZ5fh0UzIaXyP/Ef0CbKlagVX5M/2vrgkR7cGJAytkTUS6G4ok384/Jb6kCgYBe/IivlPRdZ5FFtM+1RWqFv0XZU6HP66RyP4DZ5f02j+pi8RxI9hPUjfWNDbKTU0DeLmxCT7OPzqMbs66D2Va8wV2zLaQNv2QbKNf7A1amzwkFQWtDkFXZyti/PW3LMhfdGpx5NTbY2lsfb+Nln9pN7Ab09Ljp73cRsZeJXCiV+QKBgEK/dlPQf+SEPf4Eij98U215LrpNLM7s3c8VyAxxbusqAKAvQnLTDOuw1sJ3Y+Tr1lw7Uovp+TigBE3ZyfYsh1NapI5FI4uJ9EXxS6JbuEsfjMTOa0aNEF2ejlailJXGDX7POUNH4/YgjvMLLT1M/f+ShuhxRXBeh8EMh5IbKDx7',
        'log'            => [
            'file' => storage_path('logs/alipay.log'),
        ],
    ],

    'wechat' => [
        'app_id'      => '',
        'mch_id'      => '',
        'key'         => '',
        'cert_client' => '',
        'cert_key'    => '',
        'log'         => [
            'file' => storage_path('logs/wechat_pay.log'),
        ],
    ],
];